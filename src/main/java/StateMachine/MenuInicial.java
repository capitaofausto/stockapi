package StateMachine;

public class MenuInicial implements State {

	@Override
	public int execute() {
		System.out.println("1)\t Listar produtos");
		System.out.println("2)\t Listar prateleiras");
		System.out.println("3)\t Sair");
		int [] opcoesValidas = {1,2,3};
		return scannerUtils.getValidIntFromScanner("Por favor selecione uma das anteriores opcoes: ", opcoesValidas);	
	}
	
}
