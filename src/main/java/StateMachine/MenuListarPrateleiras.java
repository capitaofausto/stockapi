package StateMachine;

public class MenuListarPrateleiras implements State {
	
	@Override
	public int execute() {
		// TODO Auto-generated method stub
		System.out.println("Lista de prateleiras:");
		System.out.println("(...)");
		System.out.println("1)\t Criar uma prateleira");
		System.out.println("2)\t Editar uma prateleira existente");
		System.out.println("3)\t Consultar os detalhes de uma prateleira");
		System.out.println("4)\t Remover uma prateleira");
		System.out.println("5)\t Voltar ao ecra anterior");
		int [] valoresValidos= {1,2,3,4,5};
		return scannerUtils.getValidIntFromScanner("Por favor selecione uma das seguintes opções: ",valoresValidos);

	}
	
}
