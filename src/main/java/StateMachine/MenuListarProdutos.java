package StateMachine;

public class MenuListarProdutos implements State {
	
	@Override
	public int execute() {
		System.out.println("Lista de produtos:");
		System.out.println("(...)");
	
		System.out.println("1)\t Criar novo produto");
		System.out.println("2)\t Editar um produto existente");
		System.out.println("3)\t Consultar os detalhes de um produto");
		System.out.println("4)\t Remover um produto");
		System.out.println("5)\t Voltar ao ecra anterior");
		int[] valoresValidos= {1,2,3,4,5};
		return scannerUtils.getValidIntFromScanner("Por favor selecione uma das seguintes opções: ",valoresValidos);
	}

}
