package StateMachine;

import repositories.ProductRepository;
import repositories.ShelfRepository;
import utils.ScannerUtils;

public interface State {
	
	public static final ScannerUtils scannerUtils = new ScannerUtils();
	
	//public static ProductRepository products  =  ProductRepository.getInstance();
	//public static ShelfRepository shelfs  =  ShelfRepository.getInstance();
	
	public int execute();

}
