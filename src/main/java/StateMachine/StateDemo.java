package StateMachine;

import java.util.Scanner;

public class StateDemo {
	
	public State[] States = { 
			new MenuInicial(), // State 0
			new MenuListarProdutos(), // State 1
			new MenuListarPrateleiras() // State 2
	};
	private int trasitionsMatrix[][] = {
			{ 1, 2 }, // state 0
			{ 0,1,1,1,0}, // state 1
			{ 0,2,2,2,0 } // state 2
	};
	
	private int currentState = 0;
	private int previousState = 0;
	

	public static void main(String[] args) {
		StateDemo fsm = new StateDemo();
		int option;

		while (true) {
			option = fsm.States[fsm.currentState].execute();
			if (fsm.currentState == 0 && option == 3) {
				System.out.println("Exit");
				break;
			}
			fsm.previousState = fsm.currentState;
			
			fsm.currentState = fsm.trasitionsMatrix[fsm.currentState][option-1];
		}
	}

}
