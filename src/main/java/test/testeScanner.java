package test;

import java.util.Scanner;

public class testeScanner {
	
    public static void main(String[] args) {
        System.out.println("Hello! What's your name?");
        
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        scanner.close(); // This line is not mandatory, but it is a good practice

        System.out.println("Hello " + name + "!!!");
    }
}
