package textinterface;

import java.util.Collection;
import java.util.Scanner;

import model.Product;
import model.Shelf;
import repositories.ProductRepository;
import repositories.ShelfRepository;
import utils.ScannerUtils;

public class TextInterface {
	
	//private static ProductRepository products  =  ProductRepository.getInstance();
	//private static ShelfRepository shelfs  =  ShelfRepository.getInstance();
	
	private static ProductRepository products  =  new ProductRepository();
    private static ShelfRepository shelfs  =  new ShelfRepository();
	
	static Scanner scanner = new Scanner(System.in);
	
	static ScannerUtils scannerUtils = new ScannerUtils();
	
	public static void showInitialMenu() {
		System.out.println("Por favor selecione uma das seguintes opções:");
		System.out.println("1) Listar produtos");
		System.out.println("2) Listar prateleiras");
		System.out.println("3) Sair");
        String option = scanner.nextLine();
        int op = 0;
        try {
        	op = Integer.parseInt(option);
        } catch (Exception e) {
        	System.out.println("Essa opção não é válida!!");
        	showInitialMenu();
        }
        System.out.println("Escolheu a opção " + op);
        switch(Integer.parseInt(option)) {
	    	case 1: showProductsMenu();
	    		break;
	    	case 2:	showPrateleirasMenu();
	    		break;
	    	case 3:	System.exit(0);
	    		break;
	        default: 
	        	System.out.println("Opção errada!!");
	        	showInitialMenu();
        }
	}
	
	public static void showProductsMenu() {
		System.out.println("Produtos:" + products.getAllEntities());
		System.out.println("Por favor selecione uma das seguintes opções:");
		System.out.println("1) Criar novo produto");
		System.out.println("2) Editar um produto existente");
		System.out.println("3) Consultar o detalhe de um produto");
		System.out.println("4) Remover um produto");
		System.out.println("5) Voltar ao ecrã anterior");
        String option = scanner.nextLine();
        int op = 0;
        try {
        	op = Integer.parseInt(option);
        } catch (Exception e) {
        	System.out.println("Essa opção não é válida!!");
        	showProductsMenu();
        }
        System.out.println("Escolheu a opção " + option);
        switch(op) {
        	case 1: newProduct();
        		break;
        	case 2: editProduct();
        		break;
        	case 3: consultProduct();
        		break;
        	case 4: removeProduct();
        		break;
        	case 5: showInitialMenu();
        		break;
        	default: 
            	System.out.println("Opção errada!!");
            	showProductsMenu();
        }
        scanner.close();
	}
	
	public static void showPrateleirasMenu() {
		System.out.println("Prateleiras:" + shelfs.getAllEntities());
		System.out.println("Por favor selecione uma das seguintes opções:");
		System.out.println("1) Criar nova prateleira");
		System.out.println("2) Editar uma prateleira existente");
		System.out.println("3) Consultar o detalhe de uma prateleira");
		System.out.println("4) Remover uma prateleira");
		System.out.println("5) Voltar ao ecrã anterior");
        String option = scanner.nextLine();
        int op = 0;
        try {
        	op = Integer.parseInt(option);
        } catch (Exception e) {
        	System.out.println("Essa opção não é válida!!");
        	showPrateleirasMenu();
        }
        switch(op) {
	    	case 1: newShelf();
	    		break;
	    	case 2: editShelf();
	    		break;
	    	case 3: consultShelf();
	    		break;
	    	case 4: removeShelf();
	    		break;
	    	case 5: showInitialMenu();
	    		break;
	    	default: 
	        	System.out.println("Opção errada!!");
	        	showPrateleirasMenu();
        }	
        scanner.close();
	}
	
	public static void newProduct() {	
		System.out.println("Qual e o id da prateleira onde está o produto?");
		long id = 0;
		while(scanner.hasNextInt() == false) {
			scanner.nextLine();
		}	
		id = scanner.nextInt();
		scanner.nextLine();
		Shelf shelf = new Shelf(0, 0);
		try {
			shelf = shelfs.getEntity(id);
			Product newProduct = new Product(10, 23,130);	
			products.createEntity(newProduct);
			shelf.setProduct(newProduct.getID());
		} catch (Exception e) {
			System.out.println(e);
		}
		showProductsMenu();
	}
	
	public static void newShelf() {
		Shelf shelf = new Shelf(10,20);
		shelfs.createEntity(shelf);
		showPrateleirasMenu();
	}
	
	public static void editProduct() {
		System.out.println("Quer editar que produto?");
		String id = "";
		while(id.length()==0)
			if(scanner.hasNextLine()) {
				id = scanner.nextLine();
			}	
		long idProduct = Long.valueOf(id).longValue();
        Product editedProduct = products.getEntity(idProduct);
        editedProduct.setDiscount(20);
        editedProduct.setIVA(23);
        editedProduct.setPVP(200);
		editedProduct.setID(editedProduct.getID());
		try {
			products.editEntity(editedProduct);
		} catch(Exception e) {
			System.out.println(e);
		}
		showProductsMenu();
	}
	
	public static void editShelf() {
		System.out.println("Ids disponiveis " + shelfs.getAllIds());
		Collection<Long> ids = shelfs.getAllIds();
		long[] validIds = ids.stream().mapToLong(l -> l).toArray();
		long idShelf = scannerUtils.getValidLongFromScanner("Que prateleira quer editar?", validIds);
		System.out.println(idShelf);
		showPrateleirasMenu();
	}
	
	public static void consultProduct() {
		System.out.println("Quer consultar que produto?");
		String id="";
		while(id.length()==0)
			if(scanner.hasNextLine()) {
				id = scanner.nextLine();
			}	
		long idProduct = Long.valueOf(id).longValue();
        System.out.println(products.getEntity(idProduct));
        showProductsMenu();
	}
	
	public static void consultShelf() {
		System.out.println("Quer consultar que prateleira?");
		String id="";
		while(id.length()==0)
			if(scanner.hasNextLine()) {
				id = scanner.nextLine();
			}	
		long idShelf = Long.valueOf(id).longValue();
        System.out.println(shelfs.getEntity(idShelf));
        showPrateleirasMenu();
	}
	
	public static void removeProduct() {
		System.out.println("Quer remover que produto?");
		String id="";
		while(id.length()==0)
			if(scanner.hasNextLine()) {
				id = scanner.nextLine();
			}	
		long idProduct = Long.valueOf(id).longValue();
        try {
        	products.removeEntity(idProduct);
        } catch(Exception e) {
			System.out.println(e);
		}
        showProductsMenu();
	}
	
	public static void removeShelf() {
		System.out.println("Quer remover que prateleira?");
		String id="";
		while(id.length()==0)
			if(scanner.hasNextLine()) {
				id = scanner.nextLine();
			}	
		long idShelf = Long.valueOf(id).longValue();
        try {
        	shelfs.removeEntity(idShelf);
        } catch(Exception e) {
			System.out.println(e);
		}
        showPrateleirasMenu();
	}
	
	public static void main(String[] args) {
		showInitialMenu();
	}

}
