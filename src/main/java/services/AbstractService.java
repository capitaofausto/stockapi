package services;

import java.util.Collection;

import javax.inject.Inject;

import model.Entity;
import repositories.EntityRepository;

public abstract class AbstractService<T extends EntityRepository<E>, E extends Entity> {
	
	@Inject
	protected T repository;
	
	public Collection<E> getAll() {
		return repository.getAllEntities();
	}
}
