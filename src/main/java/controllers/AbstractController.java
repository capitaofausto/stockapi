package controllers;

import java.util.Collection;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.Entity;
import repositories.EntityRepository;
import services.AbstractService;

public abstract class AbstractController<T extends AbstractService<R,E>, R extends EntityRepository<E> ,E extends Entity > {
	
	//@Inject
	//protected Instance<AbstractService<R, E>> service;
	
	@Inject
	protected T service;
	
	@GET
	@Path("healthcheck")
	@Produces(MediaType.TEXT_PLAIN)
	public String healthCheck() {
		return "O controller está em cima";
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<E> getAll() {
		return service.getAll();
	}
}
