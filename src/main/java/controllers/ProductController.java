package controllers;

import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.Product;
import repositories.ProductRepository;
import services.ProductService;

@Path("products")
public class ProductController extends AbstractController<ProductService, ProductRepository, Product> {
	

}
