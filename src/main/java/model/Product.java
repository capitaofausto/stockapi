package model;

import java.util.LinkedList;

public class Product extends Entity {
	
	private static final long serialVersionUID = 1L;
	
	public Product() {
		
	}
	
	public Product(int discount, int IVA, int PVP) {	
		this.shelfs = new LinkedList<Long>();
		this.discount = discount;
		this.IVA = IVA;
		this.PVP = PVP;
	}
	
	private LinkedList<Long> shelfs;
	private int discount = 0;
	private int IVA = 0;
	private int PVP = 0;
	
    public int getDiscount() {
        return discount;
    }
    
    public void setDiscount(int discount) {
        this.discount = discount;
    }
    
    public int getIVA() {
        return IVA;
    }
    
    public void setIVA(int IVA) {
        this.IVA = IVA;
    }
    
    public int getPVP() {
        return PVP;
    }
    
    public void setPVP(int PVP) {
        this.PVP = PVP;
    }

	@Override
	public String toString() {
		return "{ productID=" + this.getID() + ", shelfs=" + shelfs + ", discount=" + discount + ", IVA=" + IVA
				+ ", PVP=" + PVP + "}";
	}
}
