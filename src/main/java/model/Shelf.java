package model;

public class Shelf extends Entity {
	
	private static final long serialVersionUID = 1L;
	
	public Shelf() {
		
	}
	
	public Shelf(int capacity, int price) {
		super();
		this.capacity = capacity;
		this.productID = 0;
		this.price = price;
	}
	
	private int capacity = 0;
	private long productID = 0;
	private int price = 0;
    
    public int getCapacity() {
        return capacity;
    }
    
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
    
    public long getProduct() {
        return productID;
    }
    
    public void setProduct(long productID) {
        this.productID = productID;
    }
    
    public int getPrice() {
        return price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }

	@Override
	public String toString() {
		return "{ shelfID=" + this.getID() + ", capacity=" + capacity + ", productID=" + productID + ", price=" + price + "}";
	}
}
