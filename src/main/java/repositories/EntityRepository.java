package repositories;

import java.util.Collection;
import java.util.LinkedHashMap;

import model.Entity;

public abstract class EntityRepository<T extends Entity> {
	
	private LinkedHashMap<Long, T> t = new LinkedHashMap<Long, T>();
	private long lastID = 0;
	
	private long returnLastID() {
		return ++this.lastID;
	}
	
	public long createEntity(T entity) {
		if(entity.getID() != null) {
			throw new IllegalArgumentException("Esse objeto já tem um id atribuído");
		}
		long id = returnLastID();
		entity.setID(id);
		t.put(id, entity);
		System.out.println("Adicionou um novo produto " + t.get(id));
		return id;
	}
	
	public T getEntity(Long ID) {
		if(ID < 1 || ID > this.lastID) {
			throw new IllegalArgumentException("Não existe nenhum elemento com esse id");
		}
		return t.get(ID);
	}
	
	public Collection<T> getAllEntities() {
		return t.values();
	}
	
	public Collection<Long> getAllIds() {
		return t.keySet();
	}
	
	public void editEntity(T editedEntity) {
		if(editedEntity.getID() == null) {
			throw new IllegalArgumentException("O id atribuído está indefinido");
		}
		t.put(editedEntity.getID(), editedEntity);
		System.out.println("Editou um produto " + editedEntity);
	}
	
	public void removeEntity(Long ID) {
		if(ID < 1 || ID > this.lastID) {
			throw new IllegalArgumentException("Não existe nenhum elemento com esse id");
		}
		t.remove(ID);
	}

}
